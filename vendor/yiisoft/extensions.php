<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.14.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'execut/yii2-dropdown-content-input' => 
  array (
    'name' => 'execut/yii2-dropdown-content-input',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@execut/widget/dropdownContent' => $vendorDir . '/execut/yii2-dropdown-content-input',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput/src',
    ),
  ),
  'dvizh/yii2-tree' => 
  array (
    'name' => 'dvizh/yii2-tree',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dvizh/tree' => $vendorDir . '/dvizh/yii2-tree/src',
    ),
  ),
  'leandrogehlen/yii2-treegrid' => 
  array (
    'name' => 'leandrogehlen/yii2-treegrid',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@leandrogehlen/treegrid' => $vendorDir . '/leandrogehlen/yii2-treegrid',
    ),
  ),
  '2amigos/yii2-dosamigos-asset-bundle' => 
  array (
    'name' => '2amigos/yii2-dosamigos-asset-bundle',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@dosamigos/assets' => $vendorDir . '/2amigos/yii2-dosamigos-asset-bundle',
    ),
  ),
  '2amigos/yii2-grid-view-library' => 
  array (
    'name' => '2amigos/yii2-grid-view-library',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@dosamigos/grid' => $vendorDir . '/2amigos/yii2-grid-view-library/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
);
