<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = $news['title'];
?>
<div class="content">
    <div class="heading-title"><?= Html::encode($this->title) ?></div>
    <div class="main">
        <div id="news-item-expand">
            <h2 class="news-item-header"><?= Html::encode($news['title']) ?></h2>
            <div class="news-content"><?= $news['text'] ?></div>
            <div class="news-item-sections">
                <?php if(isset($news['sections']) && !empty($news['sections'])) { ?>
                    <?php foreach ($news['sections'] as $section) { ?>
                        <div class="section">
                            <?php if($section['is_hidden'] == true) : ?>
                                <?php if(empty($section['title'])) : ?>
                                    <?php
                                        $title = $section['text'];
                                        $title = substr($title, 0, 100);
                                        $title = substr($title, 0, strrpos($title, ' ')) . '...';
                                    ?>
                                    <a href="#section_<?= $section['id_section']?>" class="section-title collapsed" data-toggle="collapse" aria-expanded="false"><?= $title ?></a>
                                <?php else: ?>
                                    <a href="#section_<?= $section['id_section']?>" class="section-title collapsed" data-toggle="collapse" aria-expanded="false"><?=$section['title']?></a>
                                <?php endif; ?>
                                <div id="section_<?= $section['id_section'] ?>" class="section-content hidden collapse"><?= $section['text'] ?></div>
                            <?php else: ?>
                                <div class="section-content"><?= $section['text'] ?></div>
                            <?php endif; ?>

                        </div>
                    <?php } /* endif */ ?>
                <?php } /* endforeach */?>
            </div>

        </div>
    </div>
</div>
