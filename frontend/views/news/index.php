<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Новости';
?>
<div class="content">
    <div class="heading-title"><?= Html::encode($this->title) ?></div>
    <div class="main">

        <div id="news">

            <?php foreach ($newsList as $news) { ?>
                    <div class="news-item clearfix">
                        <div>
                            <h2 class="news-item-header"><a href="<?= Url::toRoute(['/news/' . Html::encode($news['id_news'])]) ?>"><?= Html::encode($news['title']) ?></a></h2>
                            <div class="row mx-0">
                                <div class="col px-0">
                                    <div class="news-item-dates">
                                        <div class="dates">
                                            <?php echo Html::encode($news['date_from']) ;?>
                                        </div>

                                        <?php if(!empty($news['sections'])){
                                                $section = $news['sections'][0];
                                            } else{
                                                $section = '';
                                            }
                                        ?>

                                        <?php if(!empty($section['images'])) : ?>
                                            <img src="/img/<?= Html::encode($section['images'][0]['path_image']) ?>" class="img-fluid" alt="">
                                        <?php else :?>
                                            <img src="/img/placeholder.png" class="img-fluid" alt="">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-7 px-0">
                                    <?php if(!empty($section)) { ?>
                                        <?php
                                            $text = Html::encode($section['text']);
                                            $text = substr($text, 0, 500);
                                            $text = substr($text, 0, strrpos($text, ' ')) . '...';

                                        ?>
                                        <p class="news-item-text"><?= $text ?></p>
                                    <?php } ?>
                                </div>
                                <a class="col-12 text-right btn_more" href="<?= Url::toRoute(['news/' . $news['id_news']]) ?>">Подробнее</a>
                            </div>

                        </div>
                     </div>
            <?php } /* endforeach news */?>

        </div>
        <?php if($page != ceil($total/$limit)){  ?>

        <div class="col-12 d-flex justify-content-center my-3 load">
            <a href="#" id="load_more" title="Загрузить больше" data-page="<?= $page + 1 ?>" class="d-inline-block load-more-icon"></a>
        </div>
        <?php } ?>
        <div class="col-12">
            <div class="pagination">
                <?php

                $adjacents = 3;
                if ($page == 0) $page = 1;
                $prev = $page - 1;
                $next = $page + 1;
                $lastpage = ceil($total/$limit);

                $lpm1 = $lastpage - 1;

                $pagination = "";
                if($lastpage > 1)
                {
                    $pagination .= "<div class=\"pagination\">";
                    //previous button
                    if ($page > 1)
                        $pagination.= "<a href='" . Url::toRoute(['', 'page' => $prev]) . "'><i class=\"fas fa-angle-left\"></i></a>";
                    else
                        $pagination.= "<span class=\"disabled\"> <i class=\"fas fa-angle-left\"></i></span>";

                    //pages
                    if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
                    {
                        for ($counter = 1; $counter <= $lastpage; $counter++)
                        {
                            if ($counter == $page)
                                $pagination.= "<span class=\"current\">$counter</span>";
                            else
                                $pagination.= "<a href='" . Url::toRoute(['', 'page' => $counter]) . "' data-page='" . $counter . "'>" . $counter . "</a>";
                        }
                    }
                    elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
                    {
                        //close to beginning; only hide later pages
                        if($page < 1 + ($adjacents * 2))
                        {
                            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                            {
                                if ($counter == $page)
                                    $pagination.= "<span class=\"current\">$counter</span>";
                                else
                                    $pagination.= "<a href='" . Url::toRoute(['', 'page' => $counter]) . "' data-page='" . $counter . "'>" . $counter . "</a>";
                            }
                            $pagination.= "...";

                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => $lpm1]) . "' data-page='" . $lpm1 . "'>". $lpm1. "</a>";
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => $lastpage]) . "' data-page='" . $lastpage . "'>".$lastpage."</a>";
                        }
                        //in middle; hide some front and some back
                        elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                        {
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => '1']) . "' data-page='1'>1</a>";
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => '2']) . "' data-page='2'>2</a>";
                            $pagination.= "...";
                            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                            {
                                if ($counter == $page)
                                    $pagination.= "<span class=\"current\">$counter</span>";
                                else
                                    $pagination.= "<a href=\"j?page=$counter\">$counter</a>";
                            }
                            $pagination.= "...";
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => $lpm1]) . "' data-page='" . $lpm1 . "'> ". $lpm1. "</a>";
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => $lastpage]) . "' data-page='" . $lastpage . "'>".$lastpage."</a>";
                        }
                        //close to end; only hide early pages
                        else
                        {
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => '1']) . "' data-page='1'>1</a>";
                            $pagination.= "<a href='" . Url::toRoute(['', 'page' => '2']) . "' data-page='2'>2</a>";
                            $pagination.= "...";
                            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                            {
                                if ($counter == $page)
                                    $pagination.= "<span class=\"current\">$counter</span>";
                                else
                                    $pagination.= "<a href='" . Url::toRoute(['', 'page' => $counter]) . "'data-page='" . $counter . "'>" . $counter . "</a>";
                            }
                        }
                    }

                    //next button
                    if ($page < $counter - 1)
                        $pagination.= "<a href='" . Url::toRoute(['', 'page' => $next]) . "'><i class=\"fas fa-angle-right\"></i></a>";
                    else
                        $pagination.= "<span class=\"disabled\"><i class=\"fas fa-angle-right\"></i></span>";
                    $pagination.= "</div>\n";
                }

                ?>
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>
