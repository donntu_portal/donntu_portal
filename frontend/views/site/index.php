<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id="main">
    <div class="row mx-0">
        <div class="col pr-3">
            <div class="content">
                <div class="heading-title">Новости</div>
                <div class="main">
                    <?php foreach ($newsList as $news) { ?>
                        <?php foreach ($news['sections'] as $section) { ?>
                            <div class="news-item clearfix">
                                <div class="news-item-header">
                                    <div class="col">
                                        <a href="<?= Url::toRoute(['news/' . $news['id_news']]) ?>" class=""><?= Html::encode($news['title']) ?></a>
                                    </div>

                                    <span class="col-auto date"><?php echo Html::encode($news['date_from']) ;?></span>
                                </div>
                                <div class="news-item-img">


                                    <?php if(!empty($section['images'])) { ?>
                                        <img src="/img/<?= Html::encode($section['images'][0]['path_image']) ?>" class="img-fluid" alt="">
                                    <?php } /* endif */?>
                                </div>
                                <?php
                                    $text = Html::encode($section['text']);
                                    $text = substr($text, 0, 200);
                                    $text = substr($text, 0, strrpos($text, ' ')) . '...';

                                ?>
                                <p class="news-item-text"><?= $text ?></p>
                                <p class="text-right"><a href="<?= Url::toRoute(['news/' . $news['id_news']]) ?>">Подробнее</a></p>
                            </div>
                        <?php } /* endforeach section*/  ?>
                    <?php } /* endforeach news */?>
                </div>
            </div>
            <a href="<?= Url::toRoute(['news/index']) ?>" class="news-href">Все новости</a>

        </div>
        <div class="col pl-3">
            <div class="col-12 px-0">
                <?php echo $this->render($calendar) ?>
                <a href="<?= Url::toRoute(['site/calendar']) ?>" class="cal-href">Открыть календарь</a>
            </div>


            <div class="col-12 px-0" id="sidebar">
                <?php for ($i = 0; $i != 3; ++$i) { ?>
                    <div class="col-12 px-0">

                    </div>
                <?php } ?>
            </div>


            <div class="col-12 px-0" id="usefull-links">
                <div class="content">
                    <div class="heading-title">Полезные ссылки</div>
                    <ul class="unstyled">
                        <li><a href="">Монитор подачи заявлений</a></li>
                        <li><a href="">Списки лиц, подавших заявления</a></li>
                        <li><a href="">Рейтинги подавших заявление</a></li>
                        <li><a href="">Оплата обучения</a></li>
                        <li><a href="">Опрос для школьников</a></li>
                        <li><a href="">Сайт университета</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->title = 'Главная'; ?>
<style>
    .main{
        padding: 0;
    }
</style>



