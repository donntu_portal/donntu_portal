<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';

$fac = array(
        'ipo' => array(
            'title' => 'Институт последипломного образования',
            'address' => 'г.Донецк, ул. Артема, 96, 3-й корпус',
            'phone' => '+38 (062) 304-96-18',
            'email' => 'ipo@donntu.org',
            'site' => 'http://ipo.donntu.org',
            'coords' => ''
        ),
        'dpi' => array(
            'title' => 'ГОУВПО «ДонНТУ»',
            'address' => 'г. Донецк, ул. Артема, 58',
            'phone' => '+38 (062) 301-07-09<br/>+38 (062) 301-07-69<br/>+38 (062) 301-08-89',
            'email' => 'donntu.info@mail.ru',
            'site' => 'http://donntu.org'
        ),
        'zaochniy' => array(
            'title' => 'Институт инновацонных технологий заочного обучения',
            'address' => 'г. Донецк, ул. Артема, 58, 1-й корпус, 4-й этаж',
            'phone' => '+38 (062) 301-07-01<br/>+38 (062) 301-07-49',
            'email' => 'yvk@zvf.donntu.org<br/>pvg@donntu.org',
            'site' => 'http://iitzo.donntu.org'
        ),
        'gorny' => array(
            'title' => 'Горный факультет',
            'address' => 'г. Донецк, ул. Артема, 58, 9-й корпус, аудитория 9.408 (4-й этаж)',
            'phone' => '+38 (062) 335-37-86<br/>+38 (062) 301-09-57<br/>+38 (062) 301-09-35',
            'email' => 'gf@donntu.org',
            'site' => '	http://gf.donntu.org'
        ),
        'gorno-geo' => array(
            'title' => 'Горно-геологический факультет',
            'address' => 'г. Донецк, ул. Артема, 58, 9-й корпус ДонНТУ, аудитория 9.404 (4-й этаж)',
            'phone' => '+38 (062) 301-03-75<br/>+38 (062) 338-09-94',
            'email' => 'ggf@mine.donntu.org',
            'site' => 'http://ggf.donntu.org'
        ),
        'fizmet' => array(
            'title' => 'Физико-металлургический факультет',
            'address' => 'г. Донецк, ул. Артема, 58, 5-й корпус, аудитория 5.157',
            'phone' => '+38 (062) 305-38-33',
            'email' => 'decanat@fizmet.donntu.org',
            'site' => 'http://fmt.donntu.org'
        ),
        'eco' => array(
            'title' => 'Факультет экологии и химической технологии',
            'address' => 'г. Донецк, пр. Б. Хмельницкого, 106, 7-й корпус',
            'phone' => '+38 (062) 338-46-43<br/>+38 (062) 301-09-91',
            'email' => 'decanat@feht.donntu.org',
            'site' => 'http://feht.donntu.org'
        ),
        'ingmeh' => array(
            'title' => 'Факультет инженерной механики и машиностроения',
            'address' => 'г. Донецк, просп. Дзержинского, 1, 6-й корпус, аудитория 6.301 (3-й этаж)',
            'phone' => '+38 (062) 304-77-23',
            'email' => 'decanat@fimm.donntu.org',
            'site' => 'http://fimm.donntu.org'
        ),
        'electro' => array(
            'title' => 'Электротехнический факультет',
            'address' => 'г. Донецк, пр. 25-летия РККА, 1, 8-й уч. корпус, к. 8.201',
            'phone' => '+38 (062) 30 10 361',
            'email' => 'decanat@elf.donntu.org</br>decanat_etf@donntu.org',
            'site' => 'http://etf.donntu.org'
        ),
        'knt' => array(
            'title' => 'Факультет компьютерных наук и технологий',
            'address' => ' г. Донецк, ул. Кобозева,17,  4-й корпус, аудитория 27 ',
            'phone' => '+38 (062) 301-08-04',
            'email' => 'decanat@cs.donntu.org',
            'site' => 'http://cs.donntu.org'
        ),
        'kita' => array(
            'title' => 'Факультет компьютерных информационных технологий и автоматики',
            'address' => 'г. Донецк, просп. 25-летия РККА 1, 8-й корпус, аудитория 8.605',
            'phone' => '+38 (062) 301-03-34',
            'email' => 'decanat@kita.donntu.org<br/>decan@kita.donntu.org',
            'site' => 'http://fkita.donntu.org'
        ),
        'ing-econom' => array(
            'title' => 'Инженерно-экономический факультет',
            'address' => 'г. Донецк, ул. Артема, 96, 3-й корпус, аудитория 3.101а',
            'phone' => '+38 (062) 337-57-68<br/>(095) 066-85-00<br/>(071) 327-71-18',
            'email' => 'ief_donntu@mail.ru',
            'site' => 'http://ief.donntu.org'
        )
);

?>
<div class="content" id="contact">
    <div class="heading-title"><?= Html::encode($this->title) ?></div>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 fac-container">
                    <div class="row mx-0">
                        <?php foreach ($fac as $k => $v) { ?>

                        <?php if($k == 'dpi') : ?>
                            <div class="col-4 faculty active" data-fac="<?= $k ?>">
                        <?php else: ?>
                            <div class="col-4 faculty " data-fac="<?= $k ?>">
                        <?php endif; ?>
                                <img src="/img/faculties/<?=$k ?>.png" alt="<?= $v['title'] ?>" class="d-block img-fluid">
                                <span class="d-block"><?= $v['title'] ?></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-12 col-md-6 map-container">
                    <div class="yandex-map-wrapper">
                        <div id="yandex-map"></div>
                    </div>
                    <div class="contact-info-wrapper mt-2">
                        <?php foreach ($fac as $k => $v) : ?>
                            <?php if($k == 'dpi') : ?>
                                <div class="contact-info active" data-fac="<?= $k ?>">
                            <?php else: ?>
                                 <div class="contact-info" data-fac="<?= $k ?>">
                            <?php endif; ?>
                                    <div class="col-12 bold mb-3 text-center"><?= $v['title'] ?></div>
                                    <div class="row mx-0">
                                        <div class="col-3 bold">Адрес:</div>
                                        <div class="col"><?= $v['address'] ?></div>
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-3 bold">Телефон:</div>
                                        <div class="col"><?php echo html_entity_decode($v['phone']) ?></div>
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-3 bold">Email:</div>
                                        <div class="col"><?php echo  html_entity_decode($v['email']) ?></div>
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-3 bold">Сайт:</div>
                                        <div class="col"><?= $v['site'] ?></div>
                                    </div>
                                    <div class="row mx-0">
                                        <div class="col-3 bold">Время работы:</div>
                                        <div class="col">С 9:00 lо 16:00</div>
                                    </div>
                                </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
     </div>
</div>
    </div>
</div>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<script>
    fac = {
        'ipo' : [48.01371112439162, 37.80102584024817],
        'dpi' : [47.994137641391866, 37.80414074640766],
        'zaochniy' : [47.994137641391866, 37.80414074640766],
        'gorny' : [47.99299519663174, 37.80349464683436],
        'gorno-geo' : [47.99299519663174, 37.80349464683436],
        'fizmet' : [47.993335472937375, 37.80252368717094],
        'eco' : [48.01200495338959,37.80720529085217],
        'ingmeh' : [47.99382481364635,37.806538172285876],
        'electro' : [47.995153097039285,37.802284049552725],
        'knt' : [47.99417293967744,37.80264388235755],
        'kita' : [47.995153097039285,37.802284049552725],
        'ing-econom' : [48.01371112439162, 37.80102584024817],
     }

</script>

<script>

    var mMap, route;
    var DPI = [47.994357, 37.803182];
    var routes = [];
    ymaps.ready(initMap);

    function initMap() {
        mMap = new ymaps.Map("yandex-map", {
            center: DPI,
            zoom: 17
        });

        myPlacemark = new ymaps.Placemark([47.994137641391866, 37.80414074640766], {
            balloonContent: 'ГОУВПО «ДонНТУ»',
            iconCaption: 'ГОУВПО «ДонНТУ»'
        }, {
            preset: 'islands#greenDotIconWithCaption',
            iconColor: '#4071b4'
        });

        mMap.geoObjects
            .add(myPlacemark);
    }

</script>

