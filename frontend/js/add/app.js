$(document).ready(function () {
    var interleaveOffset = -.5;

    var interleaveEffect = {

        onProgress: function(swiper, progress){
            for (var i = 0; i < swiper.slides.length; i++){
                var slide = swiper.slides[i];
                var translate, innerTranslate;
                progress = slide.progress;

                if (progress > 0) {
                    translate = progress * swiper.width;
                    innerTranslate = translate * interleaveOffset;
                }
                else {
                    innerTranslate = Math.abs( progress * swiper.width ) * interleaveOffset;
                    translate = 0;
                }

                $(slide).css({
                    transform: 'translate3d(' + translate + 'px,0,0)'
                });

                $(slide).find('.slide-inner').css({
                    transform: 'translate3d(' + innerTranslate + 'px,0,0)'
                });
            }
        },

        onTouchStart: function(swiper){
            for (var i = 0; i < swiper.slides.length; i++){
                $(swiper.slides[i]).css({ transition: '' });
            }
        },

        onSetTransition: function(swiper, speed) {
            for (var i = 0; i < swiper.slides.length; i++){
                $(swiper.slides[i])
                    .find('.slide-inner')
                    .andSelf()
                    .css({ transition: speed + 'ms' });
            }
        }
    };

    var swiperOptions = {
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        loop: true,
        speed: 1000,
        watchSlidesProgress: true,
        mousewheelControl: true
    };

    swiperOptions = $.extend(swiperOptions, interleaveEffect);

    var swiper = new Swiper('.swiper-container', swiperOptions);
});

var startBackTopOffset;

$(document).ready(function () {
    startBackTopOffset = parseInt($("#back-top").css("bottom"));
});

$(document).on('click', "#back-top", function () {
    $('body,html').animate({
        scrollTop: 0
    }, 800);
});

$(window).scroll(function() {
    if (window.innerWidth > 991) {
        if ($(this).scrollTop() > 200) {
            $("#back-top").addClass("is_active");
        } else {
            $("#back-top").removeClass("is_active");
        }
    }
    var scrolling = $(document).height() - $(window).height();
    var offset = $(window).scrollTop() + $(".footer").outerHeight();

    //   console.log(scrolling + "  " + offset);

    if(offset >= scrolling){
        $("#back-top").css("bottom", (startBackTopOffset + (offset - scrolling))+"px");
    } else{
        $("#back-top").css("bottom", startBackTopOffset+"px");
    }
});

$(document).on('click', '#load_more', function (e) {
    e.preventDefault();
    var page = $(this).attr('data-page');
    var _this = $(this);

    $(".pagination [data-page='" + page +"']").replaceWith($('<span class="current">' + page + '</span>'));

    $.ajax({
        url: 'news?page=' + page,
        dataType: 'html',
        cache: false,
        success : function (html) {
            var $html = $(html);
            _this.parents(".load").remove();

            $("#news").append($html.find("#news").children());
            //$(".pagination").html($html.find(".pagination").children());
            $("#news").append($html.find(".load"));
        }
    });

});

/*$(document).on('mouseenter', '#doc-list', function (e) {

});

$(document).on('mouseleave', '#doc-list', function (e) {

});*/

fac_coords = {
    'ipo' : [48.01371112439162, 37.80102584024817],
    'dpi' : [47.994137641391866, 37.80414074640766],
    'zaochniy' : [47.994137641391866, 37.80414074640766],
    'gorny' : [47.99299519663174, 37.80349464683436],
    'gorno-geo' : [47.99299519663174, 37.80349464683436],
    'fizmet' : [47.993335472937375, 37.80252368717094],
    'eco' : [48.01200495338959,37.80720529085217],
    'ingmeh' : [47.99382481364635,37.806538172285876],
    'electro' : [47.995153097039285,37.802284049552725],
    'knt' : [47.99417293967744,37.80264388235755],
    'kita' : [47.995153097039285,37.802284049552725],
    'ing-econom' : [48.01371112439162, 37.80102584024817],
};
fac_names = {
    'ipo' : 'Институт последипломного образования',
    'dpi' : 'ГОУВПО «ДонНТУ»',
    'zaochniy' : 'Институт инновацонных технологий заочного обучения',
    'gorny' : 'Горный факультет',
    'gorno-geo' : 'Горно-геологический факультет',
    'fizmet' : 'Физико-металлургический факультет',
    'eco' : 'Факультет экологии и химической технологии',
    'ingmeh' : 'Факультет инженерной механики и машиностроения',
    'electro' : 'г. Донецк, пр. 25-летия РККА, 1, 8-й уч. корпус, к. 8.201',
    'knt' : 'Факультет компьютерных наук и технологий',
    'kita' : 'Факультет компьютерных информационных технологий и автоматики',
    'ing-econom' : 'Инженерно-экономический факультет',
};


$(document).on('click', '.faculty', function () {
    $(".faculty.active").removeClass("active");
    $(this).addClass("active");

    var fac = $(this).attr("data-fac");
    $(".contact-info").removeClass("active");
    $(".contact-info[data-fac='" + fac +"']").addClass("active");
});




$(document).on('click', '.faculty', function () {
    var index = $(this).data("fac");

    mMap.setCenter(fac_coords[index], 17, {
        checkZoomRange: true
    });

    mMap.geoObjects.removeAll();

    myPlacemark = new ymaps.Placemark(fac_coords[index], {
        balloonContent: fac_names[index],
        iconCaption: fac_names[index]
    }, {
        preset: 'islands#greenDotIconWithCaption',
        iconColor: '#4071b4'
    });

    mMap.geoObjects
        .add(myPlacemark);

});
