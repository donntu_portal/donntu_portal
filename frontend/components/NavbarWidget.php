<?php

namespace app\components;
use yii\base\Widget;
use frontend\models\News;


class NavbarWidget extends Widget
{

    private $docs;
    const DOCUMENT_CATEGORY_ID = 1;

    public function init() {
        parent::init();
        $model = new News();
        $this->docs = $model->getNewsByCategoryId(self::DOCUMENT_CATEGORY_ID);
    }

    public function run() {
        return $this->render('nav', ['docs' => $this->docs]);
    }
}