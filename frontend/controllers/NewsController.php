<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 08.01.2019
 * Time: 20:11
 */

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\News;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{

    public $layout = 'main';

    public function behaviors()
    {
        return [];
    }



    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionNews($id){
        $model = new News();

        $news = $model->getNewsById($id);

        return $this->render('news', ['news' => $news]);
    }

    public function actionIndex($page = 1){
        $model = new News();

        $limit = 10;

        $filter_data = array(
            'page' => $page,
            'limit' => $limit,
        );

        $news = $model->getNews($filter_data);

        $total = $model->getTotalNews();

        foreach($news as &$item){
            foreach ($item['sections'] as &$section){
                if(empty($section['images'])){
                    $section['images'][] =
                        ['path_image' => 'placeholder.png'];
                } else {
                    foreach ($section['images'] as &$image){
                        if(!file_exists($image['path_image'])){
                            $image['path_image'] = 'placeholder.png';
                        }
                    } unset($image);
                }
            }
            unset($section);
        }unset($item);

        return $this->render("index", ["newsList" => $news, 'page'=> $page, 'limit' => $limit, 'total'=>$total]);
    }
}